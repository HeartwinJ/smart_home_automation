import 'package:flutter/material.dart';

import 'package:smart_home_automation/apiProvider.dart';

class SmartPage extends StatefulWidget {
  @override
  _SmartPageState createState() => _SmartPageState();
}

class _SmartPageState extends State<SmartPage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Smart Mode is On',
                    style: TextStyle(
                      fontSize: 28,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: RaisedButton(
                      child: Text('Turn Off'),
                      onPressed: () {
                        // Call Smart Mode OFF API here
                        ApiProvider _api = ApiProvider();
                        _api
                            .get('test2.php?smart=SMART')
                            .then((value) => print(value));
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          alignment: Alignment.bottomLeft,
          child: Card(
            elevation: 0,
            color: Colors.transparent,
            child: Text(
              'Project By:\nBavithran\nPrasana Kumar',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
