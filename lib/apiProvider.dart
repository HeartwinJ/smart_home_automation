import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class ApiProvider {
  final String _baseUrl = 'http://169.254.60.244/';

  Future<dynamic> get(String url) async {
    var _responseJson;
    try {
      final response = await http.get(_baseUrl + url);
      _responseJson = json.decode(response.body.toString());
    } catch (error) {
      print('An Error Occurred : ');
      print(error);
    }
    return _responseJson;
  }
}
