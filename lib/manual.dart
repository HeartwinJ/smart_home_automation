import 'package:flutter/material.dart';

import 'package:smart_home_automation/apiProvider.dart';

class ManualPage extends StatefulWidget {
  @override
  _ManualPageState createState() => _ManualPageState();
}

class _ManualPageState extends State<ManualPage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'LED',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          child: Text('On'),
                          onPressed: () {
                            ApiProvider _api = ApiProvider();
                            _api
                                .get('test3.php?on=ON')
                                .then((value) => print(value));
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          child: Text('Off'),
                          onPressed: () {
                            ApiProvider _api = ApiProvider();
                            _api
                                .get('test3.php?off=OFF')
                                .then((value) => print(value));
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    'Fan',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          child: Text('On'),
                          onPressed: () {
                            ApiProvider _api = ApiProvider();
                            _api
                                .get('test3.php?on1=ON1')
                                .then((value) => print(value));
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          child: Text('Off'),
                          onPressed: () {
                            ApiProvider _api = ApiProvider();
                            _api
                                .get('test3.php?off1=OFF1')
                                .then((value) => print(value));
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    'TV',
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          child: Text('On'),
                          onPressed: () {
                            ApiProvider _api = ApiProvider();
                            _api
                                .get('test3.php?on2=ON2')
                                .then((value) => print(value));
                          },
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: RaisedButton(
                          child: Text('Off'),
                          onPressed: () {
                            ApiProvider _api = ApiProvider();
                            _api
                                .get('test3.php?off2=OFF2')
                                .then((value) => print(value));
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          alignment: Alignment.bottomLeft,
          child: Card(
            elevation: 0,
            color: Colors.transparent,
            child: Text(
              'Project By:\nBavithran\nPrasana Kumar',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
