import 'package:flutter/material.dart';
import 'package:smart_home_automation/manual.dart';
import 'package:smart_home_automation/smart.dart';

import 'package:smart_home_automation/apiProvider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Home Automation',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Manual Mode",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: RaisedButton(
                      child: Text("Go to Manual Page"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ManualPage()));
                      },
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  Text(
                    "Smart Mode",
                    style: TextStyle(fontSize: 24, color: Colors.white),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: RaisedButton(
                      child: Text("Turn On"),
                      onPressed: () {
                        ApiProvider _api = ApiProvider();
                        _api
                            .get('test2.php?smart=SMART')
                            .then((value) => print(value));
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SmartPage()));
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8.0),
          alignment: Alignment.bottomLeft,
          child: Card(
            elevation: 0,
            color: Colors.transparent,
            child: Text(
              'Project By:\nBavithran\nPrasana Kumar',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
                fontStyle: FontStyle.italic,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
